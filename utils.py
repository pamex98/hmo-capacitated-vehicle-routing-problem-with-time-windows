import math
import random
from structures import *
import os
from typing import List
import json
import matplotlib.pyplot as plt
import argparse


def parse(input_file) -> Problem:
    def parse_customer_line(line):
        return [int(x.strip()) for x in line.strip().split()]

    with open(input_file, "r") as f:
        lines = f.readlines()
        vehicle_number, vehicle_capacity = parse_customer_line(lines[2])
        depot = Customer(*parse_customer_line(lines[7]))
        customers = [Customer(*parse_customer_line(lines[i])) for i in range(8, len(lines))]
    return Problem(depot, vehicle_number, vehicle_capacity, customers)


def save_checkpoint_solutions(input_filename, output_dir, solution_checkpoints: List[Solution],
                              time_checkpoints, num_of_objective_eval_checkpoints,
                              additional_output_file_tag=""):
    for solution_checkpoint, time_checkpoint in zip(solution_checkpoints, time_checkpoints):
        output_filename_no_ext = "res-" + str(time_checkpoint) + "m-" + os.path.splitext(input_filename)[
            0] + additional_output_file_tag
        output_filename = output_filename_no_ext + ".txt"
        info_filename = output_filename_no_ext + ".json"

        output_file = os.path.join(output_dir, output_filename)
        info_file = os.path.join(output_dir, info_filename)

        num_of_objective_evaluations = {str(time_eval[0]) + "m": time_eval[1] for time_eval in
                                        zip(time_checkpoints, num_of_objective_eval_checkpoints)}

        with open(info_file, "w") as f:
            json.dump(num_of_objective_evaluations, f)

        with open(output_file, "w") as f:
            f.write(str(solution_checkpoint.get_vehicle_number()))
            f.write("\n")
            for route_order, route in enumerate(solution_checkpoint.routes, 1):
                line = f"{route_order}: "
                service_start_times = get_service_start_times(solution_checkpoint.depot, route.customers)
                for i, (customer, time) in enumerate(service_start_times):
                    line += f"{customer.id}({time})"
                    if i < len(service_start_times) - 1:
                        line += "->"
                f.write(line)
                f.write("\n")
            f.write(str(solution_checkpoint.get_total_dist()))


def euclidean_customer(c1: Customer, c2: Customer):
    return euclidean_dist((c1.x_coord, c1.y_coord), (c2.x_coord, c2.y_coord))


def euclidean_dist(v1, v2):
    return ((v2[1] - v1[1]) ** 2 + (v2[0] - v1[0]) ** 2) ** 0.5


def sum_route_demand(route: Route):
    return sum_customers_demand(route.customers)


def sum_customers_demand(customers: List[Customer]):
    return sum(customer.demand for customer in customers)


def sum_customers_dist(depot, customers: List[Customer]):
    current_loc: Customer = depot
    dist = 0
    for customer in customers:
        dist += euclidean_customer(customer, current_loc)
        current_loc = customer
    dist += euclidean_customer(current_loc, depot)
    return dist


def get_service_start_times(depot: Customer, customers: List[Customer]):
    customer_times = []
    current_time = 0
    current_loc = depot
    customer_times.append((depot, current_time))
    for customer in customers:
        current_time = max(math.ceil(euclidean_customer(current_loc, customer)) + current_time, customer.ready_time)
        customer_times.append((customer, current_time))
        current_time += customer.service_time
        current_loc = customer
    current_time += math.ceil(euclidean_customer(current_loc, depot))
    customer_times.append((depot, current_time))
    return customer_times


def is_time_feasible(depot: Customer, customers: List[Customer]):
    current_time = 0
    current_loc = depot
    for customer in customers:
        current_time = max(math.ceil(euclidean_customer(current_loc, customer)) + current_time, customer.ready_time)
        if current_time > customer.due_time:
            return False
        current_time += customer.service_time
        current_loc = customer

    current_time += math.ceil(euclidean_customer(current_loc, depot))
    return True if current_time < depot.due_time else False


def visualize(solution: Solution, save_output_file=None):
    for index, route in enumerate(solution.routes, 1):
        intensity = index / (solution.get_vehicle_number())
        color = (random.random(), intensity, random.random())
        previous_customer = solution.depot
        for customer in route.customers:
            plt.plot([previous_customer.x_coord, customer.x_coord], [previous_customer.y_coord, customer.y_coord],
                     color=color)
            previous_customer = customer
        plt.plot([previous_customer.x_coord, solution.depot.x_coord],
                 [previous_customer.y_coord, solution.depot.y_coord], color=color)
    if save_output_file is not None:
        plt.savefig(save_output_file)
    plt.show()


def load_solution(solution_file, problem: Problem) -> Solution:
    routes = []
    with open(solution_file, "r") as f:
        lines = f.readlines()
        for line in lines[1:len(lines) - 1]:
            line = line.strip()
            id_time_string = line.split(":")[1].split("->")
            customers = []
            for id_time in id_time_string:
                customer_id = int(id_time.split("(")[0])
                if customer_id == 0:
                    continue
                customers.append(problem.customers[customer_id - 1])
            routes.append(Route(sum_customers_dist(problem.depot, customers), customers))
    return Solution(problem.depot, routes)


def main():
    parser = argparse.ArgumentParser(description="Visualize solution. Optionally save the plot.")
    parser.add_argument("-i", required=True, help="File containing input instance")
    parser.add_argument("-s", required=True, help="File containing solution")
    parser.add_argument("-v", help="Plot save file")
    args = parser.parse_args()

    instance_file = args.i
    solution_file = args.s
    visualize_save_file = args.v

    problem = parse(instance_file)
    solution = load_solution(solution_file, problem)
    visualize(solution, visualize_save_file)


if __name__ == "__main__":
    main()
