from solver import *
from initial_generator import *
from improvement_generator import *
from enum import Enum


class AvailableSolver(Enum):
    SEQ_ANNEALING = "seq_annealing"
    SEQ_TABU = "seq_tabu"
    RANDOM_ANNEALING = "random_annealing"
    RANDOM_TABU = "random_tabu"
    PARALLEL_ANNEALING = "parallel_annealing"
    PARALLEL_TABU = "parallel_tabu"


class SolverFactory:
    available_solvers = {
        AvailableSolver.SEQ_ANNEALING: AlphaSolver(SequentialInitialGenerator(),
                                                   SimulatedAnnealingImprovementGenerator()),
        AvailableSolver.SEQ_TABU: AlphaSolver(SequentialInitialGenerator(), TabuSearchImprovementGenerator()),
        AvailableSolver.RANDOM_ANNEALING: AlphaSolver(RandomInitialGenerator(),
                                                      SimulatedAnnealingImprovementGenerator()),
        AvailableSolver.RANDOM_TABU: AlphaSolver(RandomInitialGenerator(), TabuSearchImprovementGenerator()),
        AvailableSolver.PARALLEL_ANNEALING: AlphaSolver(ParallelInitialGenerator(),
                                                        SimulatedAnnealingImprovementGenerator()),
        AvailableSolver.PARALLEL_TABU: AlphaSolver(ParallelInitialGenerator(), TabuSearchImprovementGenerator())
    }

    def get_solver(self, available_solver: AvailableSolver):
        return self.available_solvers[available_solver]
