from abc import ABC, abstractmethod
import time
import random
from utils import *
from itertools import combinations
import copy
import collections


class ImprovementGenerator(ABC):

    @abstractmethod
    def improve(self, solution: Solution, problem: Problem, time_checkpoints: List[int]) -> (List[Solution], List[int]):
        pass

    def get_tag(self):
        return ""


class NeighborOperator(ABC):

    @abstractmethod
    def generate_neighbor(self, solution: Solution, problem: Problem) -> Solution:
        pass


@dataclass
class RandomIntraRouteExchange(NeighborOperator):
    max_locations: int = 10

    def generate_neighbor(self, s: Solution, problem: Problem) -> Solution:
        routes = copy.deepcopy(s.routes)

        target_route = random.sample(routes, 1)[0]
        target_route_len = target_route.get_len()
        target_route_indices = random.sample(range(target_route_len), min(self.max_locations, target_route_len))

        for pair in combinations(target_route_indices, 2):
            i, j = pair
            target_route_customers = target_route.customers.copy()
            target_route_customers[i], target_route_customers[j] = target_route_customers[j], target_route_customers[i]
            if is_time_feasible(s.depot, target_route_customers):
                target_route.customers = target_route_customers
                target_route.dist = sum_customers_dist(s.depot, target_route_customers)
                return Solution(s.depot, routes)

        # return unchanged if no feasible option found
        return s


@dataclass
class RandomInterRouteExchange(NeighborOperator):
    max_locations: int = 10

    def generate_neighbor(self, s: Solution, problem: Problem) -> Solution:
        routes = copy.deepcopy(s.routes)

        exchange_routes = random.sample(routes, 2)
        source_route, target_route = exchange_routes[0], exchange_routes[1]
        source_route_len, target_route_len = source_route.get_len(), target_route.get_len()

        source_route_indices = random.sample(range(source_route_len), min(source_route_len, self.max_locations))
        target_route_indices = random.sample(range(target_route_len), min(target_route_len, self.max_locations))

        for source_index in source_route_indices:
            tmp_customer = source_route.customers[source_index]
            for target_index in target_route_indices:
                source_route_customers = source_route.customers.copy()
                target_route_customers = target_route.customers.copy()
                source_route_customers[source_index] = target_route_customers[target_index]
                target_route_customers[target_index] = tmp_customer
                if sum_customers_demand(source_route_customers) > problem.capacity or sum_customers_demand(
                        target_route_customers) > problem.capacity:
                    continue
                if is_time_feasible(s.depot, source_route_customers) and is_time_feasible(s.depot,
                                                                                          target_route_customers):
                    source_route.customers, target_route.customers = source_route_customers, target_route_customers
                    source_route.dist, target_route.dist = sum_customers_dist(s.depot,
                                                                              source_route_customers), sum_customers_dist(
                        s.depot, target_route_customers)
                    return Solution(s.depot, routes)

        # return unchanged if no feasible option found
        return s


class RoutePicker(ABC):

    @abstractmethod
    def pick_route(self, routes: List[Route]) -> Route:
        pass


class MinLengthRoutePicker(RoutePicker):

    def pick_route(self, routes: List[Route]) -> Route:
        return min(routes, key=lambda x: x.get_len())


class RandomRoutePicker(RoutePicker):

    def pick_route(self, routes: List[Route]) -> Route:
        return random.choice(routes)


class LengthWiseRoutePicker(RoutePicker):

    def pick_route(self, routes: List[Route]) -> Route:
        total_routes_len = sum(route.get_len() for route in routes)
        source_route = random.choices(routes, [1 - route.get_len() / total_routes_len for route in routes], k=1)[0]
        return source_route


class FirstImprovingInterRouteExchange(NeighborOperator):

    def __init__(self, max_locations=10):
        self.max_locations = max_locations

    def generate_neighbor(self, s: Solution, problem: Problem) -> Solution:
        routes = copy.deepcopy(s.routes)

        exchange_routes = random.sample(routes, 2)
        source_route, target_route = exchange_routes[0], exchange_routes[1]
        source_route_len, target_route_len = source_route.get_len(), target_route.get_len()

        source_route_indices = random.sample(range(source_route_len), min(source_route_len, self.max_locations))
        target_route_indices = random.sample(range(target_route_len), min(target_route_len, self.max_locations))

        for source_index in source_route_indices:
            tmp_customer = source_route.customers[source_index]
            for target_index in target_route_indices:
                source_route_customers = source_route.customers.copy()
                target_route_customers = target_route.customers.copy()
                source_route_customers[source_index] = target_route_customers[target_index]
                target_route_customers[target_index] = tmp_customer
                if sum_customers_demand(source_route_customers) > problem.capacity or sum_customers_demand(
                        target_route_customers) > problem.capacity:
                    continue
                if is_time_feasible(s.depot, source_route_customers) and is_time_feasible(s.depot,
                                                                                          target_route_customers):
                    tmp_routes = copy.deepcopy(routes)
                    tmp_source_route, tmp_target_route = tmp_routes[routes.index(source_route)], tmp_routes[
                        routes.index(target_route)]
                    tmp_source_route.customers, tmp_target_route.customers = source_route_customers, target_route_customers
                    tmp_source_route.dist, tmp_target_route.dist = sum_customers_dist(s.depot,
                                                                                      source_route_customers), sum_customers_dist(
                        s.depot, target_route_customers)

                    new_solution = Solution(s.depot, tmp_routes)
                    problem.inc_objective_eval()
                    if new_solution < s:
                        return new_solution

        # return unchanged if no feasible option found
        return s


class FirstImprovingInterRouteMove(NeighborOperator):

    def __init__(self, route_picker, max_customers=10, max_routes=10, max_locations=10):
        self.max_customers = max_customers
        self.max_routes = max_routes
        self.max_locations = max_locations
        self.route_picker = route_picker

    def generate_neighbor(self, s: Solution, problem: Problem) -> Solution:
        routes = copy.deepcopy(s.routes)

        source_route = self.route_picker.pick_route(routes)
        routes.remove(source_route)

        source_route_len = source_route.get_len()
        source_route_customers = random.sample(source_route.customers,
                                               min(self.max_customers, source_route_len))
        target_routes = random.sample(routes, min(len(routes), self.max_routes))

        for source_route_customer in source_route_customers:
            for route in target_routes:
                route_demand = sum_route_demand(route)
                if (route_demand + source_route_customer.demand) > problem.capacity:
                    continue
                target_route_len = route.get_len()
                target_route_insert_indices = random.sample(range(target_route_len + 1),
                                                            min(target_route_len + 1, self.max_locations))

                for target_route_insert_index in target_route_insert_indices:
                    tmp_customers = route.customers.copy()
                    tmp_customers.insert(target_route_insert_index, source_route_customer)
                    if is_time_feasible(problem.depot, tmp_customers):
                        tmp_routes = copy.deepcopy(routes)
                        tmp_source_route = copy.deepcopy(source_route)
                        tmp_route = tmp_routes[routes.index(route)]
                        tmp_route.customers = tmp_customers
                        tmp_route.dist = sum_customers_dist(s.depot, tmp_customers)
                        tmp_source_route.customers.pop(source_route.customers.index(source_route_customer))
                        if tmp_source_route.get_len() > 0:
                            tmp_source_route.dist = sum_customers_dist(s.depot, tmp_source_route.customers)
                            tmp_routes.append(tmp_source_route)
                        new_solution = Solution(s.depot, tmp_routes)
                        problem.inc_objective_eval()
                        if new_solution < s:
                            return new_solution

        # return unchanged if no feasible option found
        return s


class FirstImprovingIntraRouteExchange(NeighborOperator):

    def __init__(self, max_locations=15):
        self.max_locations = max_locations

    def generate_neighbor(self, s: Solution, problem: Problem) -> Solution:
        routes = copy.deepcopy(s.routes)

        target_route = random.sample(routes, 1)[0]
        target_route_len = target_route.get_len()
        target_route_indices = random.sample(range(target_route_len), min(self.max_locations, target_route_len))

        for pair in combinations(target_route_indices, 2):
            i, j = pair
            target_route_customers = target_route.customers.copy()
            target_route_customers[i], target_route_customers[j] = target_route_customers[j], target_route_customers[i]
            if is_time_feasible(s.depot, target_route_customers):
                tmp_routes = copy.deepcopy(routes)
                tmp_target_route = tmp_routes[routes.index(target_route)]
                tmp_target_route.customers = target_route_customers
                tmp_target_route.dist = sum_customers_dist(s.depot, target_route_customers)
                new_solution = Solution(s.depot, tmp_routes)
                problem.inc_objective_eval()
                if new_solution < s:
                    return new_solution

        # return unchanged if no feasible option found
        return s


class RandomInterRouteMove(NeighborOperator):

    def __init__(self, route_picker, max_customers=10, max_routes=10, max_locations=10):
        self.max_customers = max_customers
        self.max_routes = max_routes
        self.max_locations = max_locations
        self.route_picker = route_picker

    def generate_neighbor(self, s: Solution, problem: Problem) -> Solution:
        routes = copy.deepcopy(s.routes)

        source_route = self.route_picker.pick_route(routes)
        routes.remove(source_route)

        source_route_len = source_route.get_len()
        source_route_customers = random.sample(source_route.customers,
                                               min(self.max_customers, source_route_len))
        target_routes = random.sample(routes, min(len(routes), self.max_routes))

        for source_route_customer in source_route_customers:
            for route in target_routes:
                route_demand = sum_route_demand(route)
                if (route_demand + source_route_customer.demand) > problem.capacity:
                    continue
                target_route_len = route.get_len()
                target_route_insert_indices = random.sample(range(target_route_len + 1),
                                                            min(target_route_len + 1, self.max_locations))
                for target_route_insert_index in target_route_insert_indices:
                    tmp_customers = route.customers.copy()
                    tmp_customers.insert(target_route_insert_index, source_route_customer)
                    if is_time_feasible(problem.depot, tmp_customers):
                        route.customers = tmp_customers
                        route.dist = sum_customers_dist(s.depot, tmp_customers)
                        source_route.customers.remove(source_route_customer)
                        if source_route.get_len() > 0:
                            source_route.dist = sum_customers_dist(s.depot, source_route.customers)
                            routes.append(source_route)
                        return Solution(s.depot, routes)

        # return unchanged if no feasible option found
        return s


class ShrinkRouteOperator(NeighborOperator):

    def __init__(self, route_picker):
        self.route_picker = route_picker

    def generate_neighbor(self, s: Solution, problem: Problem) -> Solution:
        routes = copy.deepcopy(s.routes)

        source_route = self.route_picker.pick_route(routes)
        routes.remove(source_route)

        source_route_left_customers = source_route.customers.copy()
        for customer in source_route.customers:
            placed = False
            for route in routes:
                route_demand = sum_route_demand(route)
                if (route_demand + customer.demand) > problem.capacity:
                    continue
                locations = range(route.get_len() + 1)
                for location in locations:
                    tmp_customers = route.customers.copy()
                    tmp_customers.insert(location, customer)
                    if is_time_feasible(problem.depot, tmp_customers):
                        route.customers = tmp_customers
                        route.dist = sum_customers_dist(s.depot, tmp_customers)
                        placed = True
                        source_route_left_customers.remove(customer)
                        break
                if placed:
                    break
        if len(source_route_left_customers) > 0:
            source_route.customers = source_route_left_customers
            source_route.dist = sum_customers_dist(s.depot, source_route_left_customers)
            routes.append(source_route)

        return Solution(problem.depot, routes)


@dataclass
class TabuSearchImprovementGenerator(ImprovementGenerator):
    num_of_neighbors: int = 5
    tabu_list_size: int = 20
    max_number_of_neighbor_operations_per_iter: int = 1
    neighbor_operators = [FirstImprovingInterRouteMove(LengthWiseRoutePicker()),
                          FirstImprovingInterRouteMove(MinLengthRoutePicker()),
                          FirstImprovingInterRouteMove(RandomRoutePicker()), FirstImprovingInterRouteExchange(),
                          FirstImprovingIntraRouteExchange(), ShrinkRouteOperator(MinLengthRoutePicker())]
    neighbor_operators_probabilities = [0.08, 0.08, 0.08, 0.28, 0.28, 0.2]

    def improve(self, s: Solution, problem: Problem, time_checkpoints: List[int]) -> (List[Solution], List[int]):
        num_of_objective_eval_checkpoints = []
        solution_checkpoints = []
        tabu_list = collections.deque(maxlen=self.tabu_list_size)
        s_best = s
        for time_end in time_checkpoints:
            while True:
                if time_end < time.time():
                    num_of_objective_eval_checkpoints.append(problem.num_objective_eval)
                    solution_checkpoints.append(s_best)
                    break
                neighborhood = self.generate_neighborhood(s, problem, tabu_list)
                neighbor_s = min(neighborhood)
                s = neighbor_s
                s_best = min(s, s_best)
                tabu_list.appendleft(s)
                print(
                    f"s best: actual dist =  {s_best.get_total_dist()} vehicle num = {s_best.get_vehicle_number()}, neigbor_dist = {neighbor_s.get_total_dist()}, tabu_list_size = {len(tabu_list)}")
                for i in range(self.num_of_neighbors):
                    problem.inc_objective_eval()

        return solution_checkpoints, num_of_objective_eval_checkpoints

    def get_tag(self):
        return "-tabu-" + str(self.tabu_list_size)

    def generate_neighborhood(self, s, problem, tabu_list) -> List[Solution]:
        neighborhood = []
        while len(neighborhood) < self.num_of_neighbors:
            num_of_operators = random.randint(1, self.max_number_of_neighbor_operations_per_iter)
            operators = random.choices(self.neighbor_operators, self.neighbor_operators_probabilities,
                                       k=num_of_operators)
            for operator in operators:
                s = operator.generate_neighbor(s, problem)
            if s not in neighborhood and s not in tabu_list:
                neighborhood.append(s)
        return neighborhood


@dataclass
class SimulatedAnnealingImprovementGenerator(ImprovementGenerator):
    initial_temp: float = 100000
    final_temp: float = 0.01
    alpha: float = 0.99
    num_neighbors: int = 10
    max_number_of_neighbor_operations_per_iter: int = 1
    neighbor_operators = [FirstImprovingInterRouteMove(LengthWiseRoutePicker()),
                          FirstImprovingInterRouteMove(MinLengthRoutePicker()),
                          FirstImprovingInterRouteMove(RandomRoutePicker()), FirstImprovingInterRouteExchange(),
                          FirstImprovingIntraRouteExchange(), ShrinkRouteOperator(LengthWiseRoutePicker()),
                          ShrinkRouteOperator(MinLengthRoutePicker()), ShrinkRouteOperator(RandomRoutePicker())]
    neighbor_operators_probabilities = [0.08, 0.08, 0.08, 0.28, 0.28, 0.075, 0.1, 0.025]

    def improve(self, s: Solution, problem: Problem, time_checkpoints: List[int]) -> (List[Solution], List[int]):
        num_of_objective_eval_checkpoints = []
        solution_checkpoints = []
        current_temp = self.initial_temp
        s_best = s
        for time_end in time_checkpoints:
            while True:
                if time_end < time.time():
                    num_of_objective_eval_checkpoints.append(problem.num_objective_eval)
                    solution_checkpoints.append(s_best)
                    break
                if current_temp < self.final_temp:
                    current_temp = self.initial_temp
                neighbor_s: Solution = self.generate_neighbor(s, problem)
                if neighbor_s < s:
                    s = neighbor_s
                else:
                    dist_delta = abs(s.get_total_dist() - neighbor_s.get_total_dist())
                    vehicle_delta = abs(neighbor_s.get_vehicle_number() - s.get_vehicle_number())
                    prob = math.exp(-dist_delta / current_temp) if vehicle_delta == 0 else 0
                    if random.random() < prob:
                        s = neighbor_s
                s_best = min(s, s_best)
                print(
                    f"s best: actual dist =  {s_best.get_total_dist()} vehicle num = {s_best.get_vehicle_number()}, neigbor_dist = {neighbor_s.get_total_dist()}, temp = {current_temp}")
                current_temp = self.alpha * current_temp
                problem.inc_objective_eval()
        return solution_checkpoints, num_of_objective_eval_checkpoints

    def generate_neighbor(self, s: Solution, problem: Problem) -> Solution:
        neighbors = []
        for i in range(self.num_neighbors):
            num_of_operators = random.randint(1, self.max_number_of_neighbor_operations_per_iter)
            operators = random.choices(self.neighbor_operators, self.neighbor_operators_probabilities,
                                       k=num_of_operators)
            for operator in operators:
                s = operator.generate_neighbor(s, problem)
            neighbors.append(s)
        return min(neighbors)

    def get_tag(self):
        return "-simulated_annealing-" + str(self.initial_temp)
