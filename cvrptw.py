from utils import parse, save_checkpoint_solutions
from structures import Solution
from solver_factory import AvailableSolver, SolverFactory
import argparse
import os
import time
from typing import List


def cvrptw(problem, time_checkpoints, solver_tag: AvailableSolver = AvailableSolver.SEQ_ANNEALING) -> (
        List[Solution], List[int]):
    solver = SolverFactory().get_solver(solver_tag)
    checkpoint_solutions, num_iter = solver.solve(problem, time_checkpoints)
    return checkpoint_solutions, num_iter


def main():
    parser = argparse.ArgumentParser(description="Execute Capacitated Vehicle Routing Problem with Time Windows")
    input_group = parser.add_mutually_exclusive_group(required=True)
    input_group.add_argument("-i", help="File containing input instance")
    input_group.add_argument("-d", help="Directory containing input instances")
    parser.add_argument("-o", required=True, help="Save solution checkpoints directory")
    parser.add_argument("-t", required=True, type=int, nargs="+", help="List of solution time checkpoints in minutes")
    parser.add_argument("-solver", default=AvailableSolver.SEQ_ANNEALING, type=AvailableSolver,
                        choices=list(AvailableSolver))
    args = parser.parse_args()

    output_dir = args.o
    single_input_file = args.i
    input_dir = args.d
    solver_tag = args.solver

    if input_dir is None:
        current_time = time.time()
        time_checkpoints = [current_time + t * 60 for t in args.t]
        problem = parse(single_input_file)
        checkpoint_solutions, num_iter = cvrptw(problem, time_checkpoints, solver_tag)
        save_checkpoint_solutions(os.path.basename(single_input_file), output_dir, checkpoint_solutions,
                                  args.t, num_iter)
    else:
        for file in os.listdir(input_dir):
            current_time = time.time()
            time_checkpoints = [current_time + t * 60 for t in args.t]
            input_file = os.path.join(input_dir, file)
            problem = parse(input_file)
            checkpoint_solutions, num_iter = cvrptw(problem, time_checkpoints, solver_tag)
            save_checkpoint_solutions(file, output_dir, checkpoint_solutions, args.t, num_iter)


if __name__ == "__main__":
    main()
