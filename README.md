# Capacitated Vehicle Routing Problem With Time Windows
CVRPTW belongs to the class of Vehicle Routing Problems (VRP), but, on top of the generic
VRP formulation, defines additional constraints that occur in real-life scenarios. In VRP, the
objective is to find a set of routes for a fleet of vehicles serving customers from a depot. The capacity constraint in CVRP refers to the capacity of the vehicle: the total demand of all
customers supplied on a single route must not exceed the vehicle capacity. On the other
hand, the time window constraint in VRPTW refers to the interval in which a customer must
be supplied (often called the “scheduling horizon”): all customers in a route need to be
reached within their scheduling horizons, and the whole route needs to be started and
finished within the working hours of the depot. CVRPTW includes both constraints.

![](figures/formulation.png)

# Implementation
In this project the following initial solution algorithms are implemented:

1) Greedy Sequential
2) Greedy Parallel
3) Random Parallel

Also, the following solution improvement metaheuristics are implemented:

1) Modified Simulated Annealing
2) Tabu Search

Detailed documentation can be found in docs.pdf

# How to use

1) Clone the project
2) Install all required dependencies: pip install -r requirements.txt
3) Execute 
4) Visualize

# Execute 
python cvrptw.py [-h] (-i I | -d D) -o O -t T [T ...] [-solver {AvailableSolver.SEQ_ANNEALING, AvailableSolver.SEQ_TABU, AvailableSolver.RANDOM_ANNEALING AvailableSolver.RANDOM_TABU, AvailableSolver.PARALLEL_ANNEALING, AvailableSolver.PARALLEL_TABU}]

arguments:
  1) -h, --help            show this help message and exit
  2) -i I                  File containing input instance
  3) -d D                  Directory containing input instances
  4) -o O                  Save solution checkpoints directory
  5) -t T [T ...]          List of solution time checkpoints in minutes
  6) -solver {AvailableSolver.SEQ_ANNEALING,AvailableSolver.SEQ_TABU,AvailableSolver.RANDOM_ANNEALING,AvailableSolver.RANDOM_TABU,AvailableSolver.PARALLEL_ANNEALING,AvailableSolver.PARALLEL_
TABU}


# Visualize
python utils.py [-h] -i I -s S [-v V]

arguments:
  1) -h, --help  show this help message and exit
  2) -i I        File containing input instance
  3) -s S        File containing solution
  4) -v V        Plot save file

# Example Solutions

1) Instance i1.txt - 10 routes, 100 customers:

![](figures/res-un-i1.png)

2) Instance i2.txt - 18 routes, 200 customers:

![](figures/res-un-i2.png)

3) Instance i3.txt - 37 routes, 400 customers:

![](figures/res-un-i3.png)

4) Instance i4.txt - 20 routes, 600 customers:

![](figures/res-un-i4.png)

5) Instance i5.txt - 82 routes, 800 customers:

![](figures/res-un-i5.png)

6) Instance i6.txt - 19 routes, 1000 customers:

![](figures/res-un-i6.png)




  
