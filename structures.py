from dataclasses import dataclass, field
from typing import List
from functools import total_ordering


@dataclass
class Customer:
    id: int
    x_coord: int
    y_coord: int
    demand: int
    ready_time: int
    due_time: int
    service_time: int


@dataclass
class Route:
    dist: float
    customers: List[Customer] = field(default_factory=list)

    def get_len(self):
        return len(self.customers)


@dataclass
@total_ordering
class Solution:
    depot: Customer
    routes: List[Route] = field(default_factory=list)

    def __lt__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        this_dist, this_vehicle_number = self.get_total_dist(), self.get_vehicle_number()
        other_dist, other_vehicle_number = other.get_total_dist(), other.get_vehicle_number()
        return this_vehicle_number < other_vehicle_number or (
                this_vehicle_number == other_vehicle_number and this_dist < other_dist)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        this_dist, this_vehicle_number = self.get_total_dist(), self.get_vehicle_number()
        other_dist, other_vehicle_number = other.get_total_dist(), other.get_vehicle_number()
        return this_vehicle_number == other_vehicle_number and this_dist == other_dist

    def get_total_dist(self):
        return sum(route.dist for route in self.routes)

    def get_vehicle_number(self):
        return len(self.routes)


@dataclass
class Problem:
    depot: Customer
    vehicle_number: int
    capacity: int
    customers: List[Customer]
    num_objective_eval: int = 0

    def inc_objective_eval(self):
        self.num_objective_eval += 1
