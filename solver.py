from abc import ABC, abstractmethod
from structures import Problem, Solution
from dataclasses import dataclass
from initial_generator import InitialGenerator
from improvement_generator import ImprovementGenerator
from typing import List


class Solver(ABC):

    @abstractmethod
    def solve(self, problem: Problem, time_checkpoints: List[int]) -> (List[Solution], List[int]):
        pass

    def get_tag(self):
        return ""


@dataclass
class AlphaSolver(Solver):
    initial_generator: InitialGenerator
    improvement_generator: ImprovementGenerator

    def solve(self, problem: Problem, time_checkpoints: List[int]) -> (List[Solution], List[int]):
        initial_solution = self.initial_generator.generate(problem)
        improved_solution_checkpoints, num_iter = self.improvement_generator.improve(initial_solution, problem,
                                                                                     time_checkpoints)
        return improved_solution_checkpoints, num_iter

    def get_tag(self):
        return self.initial_generator.get_tag() + self.improvement_generator.get_tag()
