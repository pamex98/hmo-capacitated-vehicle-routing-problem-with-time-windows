import math
from abc import ABC, abstractmethod
from structures import *
from utils import *
import time
import random


class InitialGenerator(ABC):

    @abstractmethod
    def generate(self, problem: Problem) -> Solution:
        pass

    def get_tag(self):
        return ""


@dataclass
class ParallelInitialGenerator(InitialGenerator):
    max_customers: int = 10
    max_routes: int = 15
    max_location: int = 5

    def get_tag(self):
        return "-parallel_init-" + str(self.max_customers) + "-" + str(self.max_routes) + "-" + str(self.max_location)

    def generate(self, problem: Problem) -> Solution:
        start = time.time()

        total_demand = sum_customers_demand(problem.customers)
        lower_bound_num_of_routes = math.ceil(total_demand / problem.capacity)
        available_customers = problem.customers.copy()
        center_customers = []
        routes: List[Route] = []

        for i in range(lower_bound_num_of_routes):
            add_route(available_customers, problem, routes, center_customers)

        while len(available_customers) > 0:
            best_customer, best_route, best_customers_list, best_customers_dist, best_fitness_delta = None, None, None, 0, float(
                "inf")
            for customer in (
                    available_customers if len(available_customers) <= self.max_customers else random.sample(
                        available_customers, self.max_customers)):
                for route in (routes if len(routes) <= self.max_routes else random.sample(routes, self.max_routes)):
                    route_demand = sum_route_demand(route)
                    if (route_demand + customer.demand) > problem.capacity:
                        continue
                    locations = range(len(route.customers) + 1)
                    for location in (
                            locations if len(locations) <= self.max_location else random.sample(locations,
                                                                                                self.max_location)):
                        tmp_customers = route.customers.copy()
                        tmp_customers.insert(location, customer)
                        if not is_time_feasible(problem.depot, tmp_customers):
                            continue
                        tmp_customers_dist = sum_customers_dist(problem.depot, tmp_customers)
                        problem.inc_objective_eval()
                        fitness_delta = tmp_customers_dist - route.dist
                        if fitness_delta < best_fitness_delta:
                            best_customer, best_route, best_customers_list, best_customers_dist, best_fitness_delta = customer, route, tmp_customers, tmp_customers_dist, fitness_delta
            if best_customer is None:
                # add new route
                add_route(available_customers, problem, routes, center_customers)
            else:
                # update best fit route
                best_route.customers = best_customers_list
                best_route.dist = best_customers_dist
                available_customers.remove(best_customer)
        print(f"elapsed time = {time.time() - start} s")
        return Solution(problem.depot, routes)


@dataclass
class SequentialInitialGenerator(InitialGenerator):
    max_customers: int = 75
    max_location: int = 15

    def get_tag(self):
        return "-sequential_init-" + str(self.max_customers) + "-" + str(self.max_location)

    def generate(self, problem: Problem) -> Solution:
        start = time.time()

        available_customers = problem.customers.copy()
        center_customers = []
        routes: List[Route] = []

        add_route(available_customers, problem, routes, center_customers)
        current_route = routes[0]

        while len(available_customers) > 0:
            best_customer, best_customers_list, best_customers_dist, best_fitness_delta = None, None, 0, float("inf")
            for customer in (
                    available_customers if len(available_customers) <= self.max_customers else random.sample(
                        available_customers, self.max_customers)):
                route_demand = sum_route_demand(current_route)
                if (route_demand + customer.demand) > problem.capacity:
                    continue
                locations = range(len(current_route.customers) + 1)
                for location in (
                        locations if len(locations) <= self.max_location else random.sample(locations,
                                                                                            self.max_location)):
                    tmp_customers = current_route.customers.copy()
                    tmp_customers.insert(location, customer)
                    if not is_time_feasible(problem.depot, tmp_customers):
                        continue
                    tmp_customers_dist = sum_customers_dist(problem.depot, tmp_customers)
                    problem.inc_objective_eval()
                    fitness_delta = tmp_customers_dist - current_route.dist
                    if fitness_delta < best_fitness_delta:
                        best_customer, best_customers_list, best_customers_dist, best_fitness_delta = customer, tmp_customers, tmp_customers_dist, fitness_delta
            if best_customer is None:
                # add new route
                add_route(available_customers, problem, routes, center_customers)
                current_route = routes[-1]
            else:
                # update route
                current_route.customers = best_customers_list
                current_route.dist = best_customers_dist
                available_customers.remove(best_customer)
        print(f"elapsed time = {time.time() - start} s")
        return Solution(problem.depot, routes)


@dataclass
class RandomInitialGenerator(InitialGenerator):
    max_routes: int = 15
    max_location: int = 15

    def get_tag(self):
        return "-random_init"

    def generate(self, problem: Problem) -> Solution:
        start = time.time()

        available_customers = problem.customers.copy()
        routes: List[Route] = []

        for customer in available_customers:
            placed = False
            for route in (routes if len(routes) <= self.max_routes else random.sample(routes, self.max_routes)):
                route_demand = sum_route_demand(route)
                if (route_demand + customer.demand) > problem.capacity:
                    continue
                locations = range(len(route.customers) + 1)
                for location in (
                        locations if len(locations) <= self.max_location else random.sample(locations,
                                                                                            self.max_location)):
                    tmp_customers = route.customers.copy()
                    tmp_customers.insert(location, customer)
                    if is_time_feasible(problem.depot, tmp_customers):
                        route.customers = tmp_customers
                        placed = True
                        break
                if placed:
                    break
            if not placed:
                route = Route(0, [customer])
                routes.append(route)

        for route in routes:
            route.dist = sum_customers_dist(problem.depot, route.customers)
        problem.inc_objective_eval()

        print(f"elapsed time = {time.time() - start} s")
        return Solution(problem.depot, routes)


def sum_dist_from_others(customer, depot, center_customers):
    dist_depot = euclidean_customer(customer, depot)
    dist_to_current_centers = 0 if len(center_customers) == 0 else sum(
        euclidean_customer(center, customer) for center in center_customers)
    total = dist_depot + dist_to_current_centers
    return total


def add_route(available_customers, problem: Problem, routes, center_customers):
    new_center = max(available_customers,
                     key=lambda x: sum_dist_from_others(x, depot=problem.depot,
                                                        center_customers=center_customers))
    center_customers.append(new_center)
    available_customers.remove(new_center)
    customers = [new_center]
    routes.append(Route(sum_customers_dist(problem.depot, customers), customers))
